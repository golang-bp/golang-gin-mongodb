
## prerequisites

## start

`go run main.go`

## health endpoint
`curl localhost:8080/health`

# Tests
## run tests
`APP_ROOT=$(pwd) go test  ./...`

## run tests (verbose)
`APP_ROOT=$(pwd) go test -v  ./...`

## run tests with coverage
`APP_ROOT=$(pwd) go test -coverpkg=./... ./...`

# Mongo Database
## Local 
### Requirements
 - docker 
 - docker compose

To start locally MongoDB and mongoexpres run:<br>
`./scripts/start-dependencies.sh`

To stop all docker containers run:<br>
`docker-compose down`

To stop all docker containers and remove containers data run:<br>
`./scripts/stop-dependencies.sh`

Add mongo uri in the .env file<br>
`echo "MONGOURI=mongodb://root:root@localhost" > .env`

## migrations

inside `migrations` folder add your migration file using the following the following pattern:
mongodb commands: https://www.mongodb.com/docs/manual/reference/command/ 

### Run
use `go run main.go` and open [http://localhost:8080/health](http://localhost:8080/health) 