package main

import (
	"users/db"
	"users/server"
)

func main() {
	db.ConnectDB()
	db.Migrate()
	r := server.SetupRouter()
	r.Run(":8080")
}
