#!/bin/bash

NC=$'\e[0m'
GREEN=$'\e[0;32m'
YELLOW=$'\e[0;33m'

docker compose up -d
MONGO_EXPRESS_URL="http://localhost:8081"
until curl -s -f -o /dev/null "$MONGO_EXPRESS_URL"
do
  echo "$YELLOW waiting for docker containers to start...$NC"
  sleep 2
done
echo "$YELLOW to follow container logs run$GREEN docker compose logs -f$NC"
echo "$YELLOW go to $MONGO_EXPRESS_URL for mongo-express"
exit