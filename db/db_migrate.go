package db

import (
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mongodb"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"log"
	"users/configs"
)

func Migrate() {

	driver, err := mongodb.WithInstance(ConnectDB(), &mongodb.Config{DatabaseName: configs.Config().MongoDatabase,
		MigrationsCollection: "migrations"})

	migrationsDir := "file://" + configs.Config().AppRootFolder + "/migrations"
	instance, err := migrate.NewWithDatabaseInstance(migrationsDir, "", driver)
	if err != nil {
		panic(err)
	}
	if err != nil {
		panic(err)
	}

	err = instance.Up()
	if err != nil {
		log.Println(err.Error())
	}
}
