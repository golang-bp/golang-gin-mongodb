package routes

import (
	"github.com/gin-gonic/gin"
	"users/controllers"
)

type HealthRoute struct {
}

func HealthRoutes(r *gin.Engine) {
	hc := controllers.NewHealthController()
	hg := r.Group("/health")
	{
		hg.GET("", hc.Health)
	}
}
