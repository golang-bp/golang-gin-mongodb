package routes

import (
	"github.com/gin-gonic/gin"
	"users/controllers/admin"
)

func AdminUserRoutes(r *gin.Engine) {
	auc := admin.NewAdminUsersController()
	aug := r.Group("/admin")
	{
		aug.POST("/users", auc.CreateUser)
		aug.GET("/users", auc.GetUsers)
		aug.GET("/users/:userId", auc.GetUser)
	}
}
