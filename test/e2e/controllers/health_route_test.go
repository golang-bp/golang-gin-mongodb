package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"users/controllers"
)

func SetupRouter() *gin.Engine {
	os.Setenv("PROFILE", "test")
	hc := controllers.NewHealthController()

	r := gin.Default()
	r.GET("/health", hc.Health)
	gin.SetMode(gin.TestMode)
	return r
}

func TestHealthRoute(t *testing.T) {

	r := SetupRouter()

	req, _ := http.NewRequest("GET", "/health", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	var response gin.H
	json.Unmarshal(w.Body.Bytes(), &response)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotNil(t, response["startedAt"])
}
