package admin

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"
	"users/configs"
	"users/db"
	"users/dto"
	"users/server"
)

func SetUp() *gin.Engine {
	os.Setenv("PROFILE", "test")

	//drop existing DB
	err := db.ConnectDB().Database(configs.Config().MongoDatabase).Drop(context.TODO())
	if err != nil {
		log.Panic("Database", configs.Config().MongoDatabase, "could not be dropped")
	}

	db.Migrate()
	r := server.SetupRouter()
	return r
}

func TestGetUsers(t *testing.T) {
	r := SetUp()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/admin/users", nil)
	r.ServeHTTP(w, req)

	var response []dto.AdminUserDto
	json.Unmarshal(w.Body.Bytes(), &response)
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, 2, len(response))
}

func TestGetOneUser(t *testing.T) {
	r := SetUp()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/admin/users", nil)
	r.ServeHTTP(w, req)

	var response []dto.AdminUserDto
	json.Unmarshal(w.Body.Bytes(), &response)

	userId := response[0].Id

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/admin/users/"+userId, nil)
	r.ServeHTTP(w, req)

	var adminUserDto dto.AdminUserDto
	json.Unmarshal(w.Body.Bytes(), &adminUserDto)
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, userId, adminUserDto.Id)
}

func TestAdminCreateUser(t *testing.T) {
	r := SetUp()

	newUserDto := dto.NewUserDto{
		Email:    "e2e_user@test.com",
		Password: "mock@123", DateOfBirth: dto.CivilTime{Time: time.Now()},
	}
	w := httptest.NewRecorder()
	bytes, _ := json.Marshal(newUserDto)
	req, _ := http.NewRequest("POST", "/admin/users", strings.NewReader(string(bytes)))
	r.ServeHTTP(w, req)

	assert.Equal(t, 201, w.Code)
}
