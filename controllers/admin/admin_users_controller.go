package admin

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
	"time"
	"users/dto"
	"users/models"
	"users/repository"
)

type UsersController struct {
	validator       *validator.Validate
	usersRepository *repository.UsersRepository
}

func NewAdminUsersController() *UsersController {
	return &UsersController{
		validator:       validator.New(),
		usersRepository: repository.NewUserRepository(),
	}
}

func (uc UsersController) CreateUser(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var user dto.NewUserDto
	defer cancel()

	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if validationErr := uc.validator.Struct(&user); validationErr != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": validationErr.Error()})
		return
	}

	encryptedPass, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	userModel := models.User{
		ID:          primitive.NewObjectID(),
		Email:       user.Email,
		Password:    string(encryptedPass),
		DateOfBirth: user.DateOfBirth.Time,
		Enabled:     false,
	}
	_, err := uc.usersRepository.CreateUser(ctx, &userModel)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.Status(http.StatusCreated)
	return
}

func (uc UsersController) GetUsers(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	size, _ := strconv.ParseInt(c.DefaultQuery("size", "10"), 10, 64)
	offset, _ := strconv.ParseInt(c.DefaultQuery("offset", "0"), 10, 64)

	dbUsers, err := uc.usersRepository.GetUsers(ctx, size, offset)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
	}
	var adminUserDtos []dto.AdminUserDto
	for _, dbUser := range dbUsers {
		userDto := dto.AdminUserDto{
			Id:          dbUser.ID.Hex(),
			Email:       dbUser.Email,
			DateOfBirth: dto.CivilTime{Time: dbUser.DateOfBirth},
			Enabled:     dbUser.Enabled,
		}
		adminUserDtos = append(adminUserDtos, userDto)
	}
	c.JSON(http.StatusOK, adminUserDtos)
}

func (uc UsersController) GetUser(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	userId := c.Param("userId")

	dbUser, err := uc.usersRepository.GetUser(ctx, userId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
	}

	userDto := dto.AdminUserDto{
		Id:          dbUser.ID.Hex(),
		Email:       dbUser.Email,
		DateOfBirth: dto.CivilTime{Time: dbUser.DateOfBirth},
		Enabled:     dbUser.Enabled,
	}
	c.JSON(http.StatusOK, userDto)
}
