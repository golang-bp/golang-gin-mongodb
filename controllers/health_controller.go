package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

type HealthController struct {
	StartedAt time.Time
}

func NewHealthController() *HealthController {
	return &HealthController{
		StartedAt: time.Now(),
	}
}

func (h *HealthController) Health(ctx *gin.Context) {
	ctx.IndentedJSON(http.StatusOK, gin.H{"status": "UP", "startedAt": h.StartedAt})
}
