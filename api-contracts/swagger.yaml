openapi: 3.0.3
info:
  title: Users Service - OpenAPI 3.0
  description: |-
    Describes API contract for Users Service
  contact:
    email: denis.chiosa@reigncode.dev
  license:
    name: Feel free to use it for inspiring and contribute
  version: 0.0.1
servers:
  - url: http://localhost:8080
paths:
  /admin/users:
    post:
      tags:
        - users
      summary: register a new user
      description: as an admin, register new user into the database
      operationId: createUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewUserDto'
        description: Save a new user
      responses:
        201:
          description: Successful operation
        401:
          $ref: '#/components/responses/Unauthorized'
        400:
          $ref: '#/components/responses/BadRequest'
        500:
          $ref: '#/components/responses/ServerError'
    get:
      tags:
        - users
      summary: list all users
      description: as an admin, list all users from the database
      operationId: getUsers
      parameters:
        - name: size
          in: query
          description: page size
          required: false
          schema:
            type: integer
            format: int32
            default: 1
        - name: offset
          in: query
          description: offset
          required: false
          schema:
            type: integer
            format: int32
            default: 20
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/AdminUserDto'
        401:
          $ref: '#/components/responses/Unauthorized'
        400:
          $ref: '#/components/responses/BadRequest'
        500:
          $ref: '#/components/responses/ServerError'
  /admin/users/{userId}:
    get:
      tags:
        - users
      parameters:
        - $ref: '#/components/parameters/page'
        - $ref: '#/components/parameters/per-page'
      responses:
        200:
          description: Get one user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AdminUserDto'
        401:
          $ref: '#/components/responses/Unauthorized'
        400:
          $ref: '#/components/responses/BadRequest'
        500:
          $ref: '#/components/responses/ServerError'
components:
  parameters:
    page:
        in: query
        name: page
        description: The page to return. Defaults to 1.
        schema:
          type: integer
          format: int32
          example: 1
          default: 1
    per-page:
      in: query
      name: per-page
      description: The desired size of result pages. Defaults to 100.
      schema:
        type: integer
        format: int64
        example: 0
        default: 100
  schemas:
    NewUserDto:
      type: object
      properties:
        email:
          description: the user's email
          type: string
          format: email
          example: mock@mock-domain.com
        password:
          description: the user's password
          type: string
          example: superSecret!321
        dateOfBirth:
         $ref: '#/components/schemas/AppDate'
    AdminUserDto:
      type: object
      properties:
        id:
          description: the user's id
          type: string
          example: 63e4cdb250eb9990b465645d
        email:
          description: the user's email
          type: string
          format: email
          example: mock@mock-domain.com
        dateOfBirth:
          $ref: '#/components/schemas/AppDate'
        enabled:
          description: indicates if user is enabled or not
          type: boolean
          example: true
    AppDate:
        type: string
        format: date
  responses:
    Unauthorized:
      description: 'Unauthorized'
    BadRequest:
      description: 'Bad Request'
    ServerError:
      description: 'Server error'