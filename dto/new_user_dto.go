package dto

import (
	"time"
)

type NewUserDto struct {
	Email       string    `json:"email" validate:"required"`
	Password    string    `json:"password" validate:"required"`
	DateOfBirth CivilTime `json:"dateOfBirth"`
}

type CivilTime struct {
	time.Time
}

func (c *CivilTime) UnmarshalJSON(b []byte) (err error) {
	date, err := time.Parse(`"2006-01-02"`, string(b))
	if err != nil {
		return err
	}
	c.Time = date
	return
}

func (c CivilTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + c.Time.Format("2006-01-02") + `"`), nil
}
