package dto

type AdminUserDto struct {
	Id          string    `json:"id"`
	Email       string    `json:"email" validate:"required"`
	DateOfBirth CivilTime `json:"dateOfBirth"`
	Enabled     bool      `json:"enabled"`
}
