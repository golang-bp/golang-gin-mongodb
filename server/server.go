package server

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"users/configs"
	"users/routes"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowOrigins = configs.Config().CorsAllowedOrigins
	r.Use(cors.New(config))

	routes.HealthRoutes(r)
	routes.AdminUserRoutes(r)
	return r
}
