package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type User struct {
	ID          primitive.ObjectID `bson:"_id"`
	Email       string             `bson:"email"`
	Password    string             `bson:"password"`
	DateOfBirth time.Time          `bson:"dateOfBirth"`
	Enabled     bool               `bson:"enabled"`
}
