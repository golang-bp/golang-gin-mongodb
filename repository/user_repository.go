package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"users/configs"
	"users/db"
	"users/models"
)

type UsersRepository struct {
	usersCollection *mongo.Collection
}

func NewUserRepository() *UsersRepository {
	var userCollection = db.ConnectDB().Database(configs.Config().MongoDatabase).Collection("users")
	return &UsersRepository{
		usersCollection: userCollection,
	}
}

func (ur UsersRepository) CreateUser(ctx context.Context, user *models.User) (interface{}, error) {
	res, err := ur.usersCollection.InsertOne(ctx, user)
	if err != nil {
		return "invalid", err
	}

	return res.InsertedID, nil
}

func (ur UsersRepository) GetUsers(ctx context.Context, size int64, offset int64) ([]models.User, error) {
	res, err := ur.usersCollection.Find(ctx, bson.D{}, options.Find().SetLimit(size).SetSkip(offset))
	defer res.Close(ctx)

	if err != nil {
		return nil, err
	}

	var users []models.User
	for res.Next(ctx) {
		var singleUser models.User
		err := res.Decode(&singleUser)
		if err != nil {
			return nil, err
		}
		users = append(users, singleUser)
	}
	return users, nil
}

func (ur UsersRepository) GetUser(ctx context.Context, userId string) (*models.User, error) {
	var user models.User
	objectId, _ := primitive.ObjectIDFromHex(userId)
	err := ur.usersCollection.FindOne(ctx, bson.M{"_id": objectId}).Decode(&user)
	if err != nil {
		return nil, err
	}

	return &user, err
}
