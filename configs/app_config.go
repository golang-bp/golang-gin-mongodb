package configs

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

type AppConfig struct {
	AppRootFolder      string
	MongoURI           string
	MongoDatabase      string
	CorsAllowedOrigins []string
}

var instance *AppConfig

func Config() *AppConfig {
	env := os.Getenv("PROFILE")
	log.Println("current profile is", env)
	switch env {
	case "test":
		return loadTestConfig()
	case "prod":
		return loadProdConfig()
	}
	log.Println("no profile set. Loading Dev Config")
	return loadDevConfig()
}

func loadDevConfig() *AppConfig {
	if instance != nil {
		return instance
	}

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	instance = &AppConfig{
		AppRootFolder:      os.Getenv("APP_ROOT"),
		MongoURI:           os.Getenv("MONGO_URI"),
		MongoDatabase:      os.Getenv("MONGO_DB"),
		CorsAllowedOrigins: strings.Split(os.Getenv("CORS_ALLOWED_ORIGINS"), ";"),
	}
	return instance
}

func loadProdConfig() *AppConfig {
	return &AppConfig{
		AppRootFolder:      os.Getenv("APP_ROOT"),
		MongoURI:           os.Getenv("MONGO_URI"),
		MongoDatabase:      os.Getenv("MONGO_DB"),
		CorsAllowedOrigins: strings.Split(os.Getenv("CORS_ALLOWED_ORIGINS"), ";"),
	}
}
func loadTestConfig() *AppConfig {
	f := os.Getenv("APP_ROOT")
	fmt.Println(f)
	return &AppConfig{
		AppRootFolder:      os.Getenv("APP_ROOT"),
		MongoURI:           "mongodb://root:root@localhost",
		MongoDatabase:      "users_test",
		CorsAllowedOrigins: []string{"*"},
	}
}
